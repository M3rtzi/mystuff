import click
from . import cli, get_resource, bcolors, get_collection


@cli.command('find', help='Find database resources.')
@click.argument('resource')
@click.argument('attrs', nargs=-1)
@click.option('-p', '--page', default=1)
@click.option('-s', '--size', default=10)
@get_resource('resource')
def find(resource, attrs, page, size):
    query = resource.query.filter_by(**dict(map(lambda val: val.split('='), attrs)))
    callback = lambda: click.prompt(bcolors.OKGREEN + 'Next page? y/n') == 'y'
    for resource in get_collection(query=query, callback=callback, page=page, size=size):
        click.echo(bcolors.OKBLUE + str(resource))
