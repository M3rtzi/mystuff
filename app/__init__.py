from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
import boto3


db = SQLAlchemy()
migrate = Migrate()
from . import models
cors = CORS()


session = boto3.session.Session()
s3 = session.client(
    's3',
    region_name=Config.STORAGE_REGION,
    endpoint_url=Config.STORAGE_ENDPOINT,
    aws_access_key_id=Config.STORAGE_ACCESS_KEY,
    aws_secret_access_key=Config.STORAGE_SECRET_KEY
)


def create_app(config_class=Config):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    cors.init_app(app, resources={r'/*': {'origins': '*'}})

    from .api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/v1')

    #from .errors import bp as errors_bp
    #app.register_blueprint(errors_bp)

    return app

