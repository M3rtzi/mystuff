from . import bp
from .auth import basic_auth, token_auth
from app import db
from flask import jsonify


@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
    user = basic_auth.current_user()
    token = user.get_token()
    db.session.commit()
    return jsonify({
        **user.to_response_object(),
        **{'token': token, 'token_expiration': user.token_expiration}
    })


@bp.route('/tokens', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    token_auth.current_user().revoke_token()
    db.session.commit()
    return '', 204