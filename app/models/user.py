from app import db
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
import base64
import os
from . import BaseMixin
from flask import url_for


class User(BaseMixin, db.Model):
    """
    User table. Used to auth user and relate accounts to projects and assets.
    """
    from_dict_params = ['email', 'password']
    to_dict_params = ['id', 'email']
    # ID
    id = db.Column(db.Integer, primary_key=True)
    # Data
    email = db.Column(db.String(256), nullable=False)
    password_hash = db.Column(db.String(256), nullable=False)
    token = db.Column(db.String(64), unique=True, index=True)
    token_expiration = db.Column(db.DateTime)
    # Relations
    projects = db.relationship('Project', backref='user', lazy='dynamic')
    assets = db.relationship('Asset', backref='user', lazy='dynamic')

    def set_password(self, pwd):
        self.password_hash = generate_password_hash(pwd)

    def check_password(self, pwd):
        return check_password_hash(self.password_hash, pwd)

    def reset_password(self, pwd):
        self.set_password(pwd)
        return pwd

    def get_token(self, expires_in=1800):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def from_dict(self, data):
        for field in ['email', 'notifications']:
            if field in data:
                setattr(self, field, data[field])
        if 'password' in data:
            self.set_password(data['password'])

    def to_response_object(self):
        return {
            **self.to_dict(),
            '_links': {
                '_self': url_for('api.get_user'),
                '_projects': url_for('api.get_project')
            }
        }