from . import bp
from .auth import token_auth
from flask import request, jsonify, abort, current_app
from app.models import Asset
from app import db, s3
from botocore.exceptions import ClientError


@bp.route('/asset', methods=['POST'])
@token_auth.login_required
def create_asset():
    user = token_auth.current_user()
    file = request.files['file']
    key = '{project_id}/{filename}'.format(project_id=request.form['project_id'], filename=file.filename)

    asset = Asset()
    asset.from_dict({
        'user_id': user.id,
        'edge': 'https://mystuff.ams3.cdn.digitaloceanspaces.com/{filepath}'.format(filepath=key),
        'key': key,
        **request.form
    })
    db.session.add(asset)
    db.session.commit()

    s3.upload_fileobj(
        file.stream, current_app.config['STORAGE_BUCKET'], key,
        ExtraArgs={
            'ContentType': file.content_type,
            'ACL': 'public-read',
            'Metadata': {key: str(value) for key, value in asset.to_dict().items()}
        }
    )
    return jsonify(
        asset.to_response_object()
    )


@bp.route('/asset/<int:asset_id>', methods=['PUT'])
@token_auth.login_required
def put_asset(asset_id):
    user = token_auth.current_user()
    asset = Asset.query.get_or_404(asset_id)
    if user.id != asset.user_id:
        abort(403)

    file = request.files['file']
    key = '{project_id}/{filename}'.format(project_id=request.form['project_id'] or asset.project_id, filename=file.filename)
    prev_key = asset.key

    asset.from_dict({
        'user_id': user.id,
        'edge': 'https://mystuff.ams3.cdn.digitaloceanspaces.com/{filepath}'.format(filepath=key),
        'key': key,
        **request.form
    })
    db.session.commit()

    if file:
        s3.upload_fileobj(
            file.stream, current_app.config['STORAGE_BUCKET'], key,
            ExtraArgs={
                'ContentType': file.content_type,
                'ACL': 'public-read',
                'Metadata': {key: str(value) for key, value in asset.to_dict().items()}
            }
        )
    if prev_key != key:
        s3.delete_object(current_app.config['STORAGE_BUCKET'], prev_key)

    return jsonify(
        asset.to_response_object()
    )


@bp.route('/asset', methods=['GET'])
@token_auth.login_required
def get_asset():
    kwargs = {
        key: value for key, value in {
            'id': request.args.get('asset_id'),
            'project_id': request.args.get('project_id'),
            'user_id': request.args.get('user_id')
        }.items() if value is not None
    }
    query = Asset.query.filter_by(**kwargs)

    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 50, type=int), 100)
    return jsonify(
        Asset.to_collection_dict(
            query,
            page,
            per_page,
            'api.get_project'
        )
    )


@bp.route('/asset/<int:asset_id>', methods=['DELETE'])
@token_auth.login_required
def delete_asset(asset_id):
    user = token_auth.current_user()
    asset = Asset.query.get_or_404(asset_id)
    if user.id != asset.user_id:
        abort(403)
    s3.delete_object(current_app.config['STORAGE_BUCKET'], asset.key)
    asset.delete()
    db.session.commit()
    return '', 204

