import click
from app import db
from . import cli, get_resource, bcolors
from sqlalchemy.orm.exc import UnmappedInstanceError


@cli.command('delete', help='Delete database resources.')
@click.argument('resource')
@click.argument('id', nargs=-1)
@get_resource('resource')
def delete(resource, id):
    obj = resource.query.get(id)
    try:
        db.session.delete(obj)
        db.session.commit()
        click.echo(bcolors.OKBLUE + str(obj) + ' DELETED.')
    except UnmappedInstanceError:
        click.echo(bcolors.FAIL + 'No {resource} with id: {id}'.format(resource=resource.__tablename__, id=id))
