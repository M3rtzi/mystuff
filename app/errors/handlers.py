from . import bp, error_response
from app import db


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return error_response(500)
