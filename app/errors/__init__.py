from flask import Blueprint
from werkzeug.http import HTTP_STATUS_CODES
from flask import jsonify


bp = Blueprint('errors', __name__)


def error_response(status_code, msg=None):
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if msg:
        payload['msg'] = msg
    response = jsonify(payload)
    response.status_code = status_code
    return response


def bad_request(msg):
    return error_response(400, msg)


from .handlers import *