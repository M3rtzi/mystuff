from .auth import token_auth
from . import bp


@bp.route('/user', methods=['POST'])
def create_user():
    return 'Not implemented', 501


@bp.route('/user/<int:user_id>', methods=['PUT'])
@token_auth.login_required
def put_user(user_id):
    return 'Not implemented', 501


@bp.route('/user', methods=['GET'])
@token_auth.login_required
def get_user():
    return 'Not implemented', 501


@bp.route('/user', methods=['DELETE'])
@token_auth.login_required
def delete_user():
    return 'Not implemented', 501