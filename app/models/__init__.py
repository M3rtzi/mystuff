from app import db
from datetime import datetime
from sqlalchemy import inspect
from flask import url_for
from math import ceil


class BaseMixin(object):
    from_dict_params = []
    to_dict_params = []

    created = db.Column(db.DateTime, default=datetime.utcnow)
    updated = db.Column(db.DateTime, onupdate=datetime.utcnow)
    _deleted = db.Column(db.DateTime)

    def soft_delete(self):
        self._deleted = datetime.utcnow()

    def to_dict(self):
        return {key: getattr(self, key, None) for key in self.to_dict_params}

    def from_dict(self, data):
        for field in self.from_dict_params:
            if field in data:
                setattr(self, field, data[field])

    def __str__(self):
        return '<{0} {1}>'.format(
            self.__class__.__name__,
            ' '.join([str(getattr(self, key.name)) for key in inspect(self.__class__).primary_key])
        )

    def __repr__(self):
        return '{0}(**{1})'.format(
            self.__class__.__name__,
            {key: getattr(self, key) for key in self.__table__.columns.keys()}
        )


class PaginatedAPIMixin(object):
    """
    Basic pagination mixin
    """
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        return {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
                'first': url_for(endpoint, page=1, per_page=per_page, **kwargs),
                'prev': url_for(endpoint, page=page - 1, per_page=per_page, **kwargs) if resources.has_prev else None,
                'next': url_for(endpoint, page=page + 1, per_page=per_page, **kwargs) if resources.has_next else None,
                'last': url_for(endpoint, page=ceil(resources.total / per_page), per_page=per_page, **kwargs) if resources.has_next else None
            }
        }

from .project import *
from .asset import *
from .user import *