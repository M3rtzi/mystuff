import os
from dotenv import load_dotenv
from pathlib import Path


env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')

    SQLALCHEMY_DATABASE_URI = os.environ.get('DB_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {
        'pool_recycle': 900
    }

    STORAGE_REGION = os.environ.get('STORAGE_REGION') or 'ams3'
    STORAGE_BUCKET = os.environ.get('STORAGE_SPACE') or 'mystuff'
    STORAGE_ENDPOINT = os.environ.get('STORAGE_ENDPOINT') or 'https://{region}.digitaloceanspaces.com'.format(region=STORAGE_REGION)
    STORAGE_ACCESS_KEY = os.environ.get('STORAGE_ACCESS_KEY')
    STORAGE_SECRET_KEY = os.environ.get('STORAGE_SECRET_KEY')
