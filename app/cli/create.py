import click
from app import db
from . import cli, get_resource, bcolors
from sqlalchemy.exc import IntegrityError


@cli.command('create', help='Create database resources.')
@click.argument('resource')
@click.argument('data', nargs=-1)
@get_resource('resource')
def create(resource, data):
    resource = resource()
    resource.from_dict(dict(map(lambda val: val.split('='), data)))
    db.session.add(resource)
    try:
        db.session.commit()
        click.echo(bcolors.OKBLUE + str(resource) + ' CREATED')
    except IntegrityError as e:
        click.echo(bcolors.FAIL + '{resource} - {orig}'.format(resource=resource, orig=e.orig))
        db.session.rollback()
