from app import db
from . import BaseMixin, PaginatedAPIMixin


class Asset(BaseMixin, PaginatedAPIMixin, db.Model):
    """
    Asset table. Used to track and describe static assets.
    """
    from_dict_params = ['origin', 'edge', 'info', 'project_id', 'user_id']
    to_dict_params = [*from_dict_params, 'id']
    # ID
    id = db.Column(db.Integer, primary_key=True)
    # Foreign Keys
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True, nullable=False)
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'), index=True, nullable=False)
    # Data
    edge = db.Column(db.String(1024), nullable=False)
    info = db.Column(db.String(1024), nullable=False)
    key = db.Column(db.String(1024), nullable=False)
    # Relations

    def to_response_object(self):
        return {
            **self.to_dict(),
            '_links:': ''
        }
