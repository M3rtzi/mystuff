import click
from flask.cli import FlaskGroup
from app import db, create_app
import sys


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def get_resource(key):
    def wrap(func):
        def wrapped(*args, **kwargs):
            try:
                kwargs[key] = {k.lower(): v for k, v in db.Model._decl_class_registry.items()}[kwargs[key]]
                func(*args, **kwargs)
            except KeyError:
                click.echo(bcolors.FAIL + 'ERROR - No {key} "{name}"'.format(key=key, name=kwargs[key]))
        return wrapped
    return wrap


def get_collection(query, callback=None, page=1, size=20):
    resources = query.paginate(page, size, False)
    pages = resources.pages
    while page <= pages:
        for resource in resources.items:
            yield resource
        if page >= pages or (callback and not callback()):
            break
        page += 1
        resources = query.paginate(page, size, False)


def pretty(resource):
    click.echo(bcolors.OKBLUE + '{')
    for key, val in resource.__dict__.items():
        click.echo(bcolors.OKBLUE + '\t' + key + ': ' + str(val))
    click.echo(bcolors.OKBLUE + '}')


@click.group(cls=FlaskGroup, create_app=create_app, help="")
def cli():
    """MyStuff Cli"""
    pass


from . import create, find, describe, delete
