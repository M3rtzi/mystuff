import click
from . import cli, get_resource, bcolors, pretty


@cli.command('describe', help='Describe a database resources.')
@click.argument('resource')
@click.argument('id', nargs=-1)
@get_resource('resource')
def describe(resource, id):
    obj = resource.query.get(id)
    try:
        pretty(obj)
    except AttributeError:
        click.echo(bcolors.FAIL + 'No {resource} with id: {id}'.format(resource=resource.__tablename__, id=id))
