import setuptools


setuptools.setup(
    name='mystuff',
    version='0.0.1',
    author='Jakob Lilliemarck',
    author_email='lilliemarckjakob@gmail.com',
    description='',
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'mys=app.cli:cli'
        ],
    },
    python_requires='>=3.6',
)