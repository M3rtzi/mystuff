FROM registry.gitlab.com/m3rtzi/mystuff:base
MAINTAINER Jakob Lilliemarck <lilliemarckjakob@gmail.com>

#WORKDIR /mystuff

COPY app app
COPY migrations migrations
COPY config.py setup.py boot.sh mystuff.py ./

RUN venv/bin/pip install -e .
RUN chmod a+x boot.sh

# todo: trying mystuff:app to refer to factory instead of file mystuff.py in root
ENV FLASK_APP mystuff:app
USER mystuff
EXPOSE 8080
ENTRYPOINT ['boot.sh']
